﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Dynamic;
using System.Web.Http;
using System.Data.Entity;
using INF354_HW1.Models;
using System.Web.Mvc;

namespace INF354_HW1.Controllers
{
    public class HomeController : ApiController
    {
        [System.Web.Http.Route("api/Customer/getAllFirst_Names")]
        [System.Web.Mvc.HttpPost]
        public List<dynamic> getAllFirst_Names()
        {
            SALESEntities db = new SALESEntities();
            db.Configuration.ProxyCreationEnabled = false;
            return getFirst_NameReturnList(db.CUSTOMERs.ToList());
        }
        private List<dynamic> getFirst_NameReturnList(List<CUSTOMER> forSeller)
        {
            List<dynamic> dynamicCUSTOMERs = new List<dynamic>();
            foreach (CUSTOMER customer in forSeller)
            {
                dynamic dynamicCUSTOMER = new ExpandoObject();
                dynamicCUSTOMER.Customer_ID = customer.Customer_ID;
                dynamicCUSTOMER.First_Name = customer.First_Name;
                dynamicCUSTOMER.Last_Name = customer.Last_Name;
                dynamicCUSTOMER.Number_Of_Complaints = customer.Number_Of_Complaints;
                dynamicCUSTOMERs.Add(dynamicCUSTOMER);

            }
            return dynamicCUSTOMERs;

        }
        [System.Web.Http.Route("api/Customer/getAllFirst_NameWithUnitPrice")]
        [System.Web.Mvc.HttpPost]
        public List<dynamic> getAllFirst_NameWithUnitPrice()
        {
            SALESEntities db = new SALESEntities();
            db.Configuration.ProxyCreationEnabled = false;
            List<CUSTOMER> customers = db.CUSTOMERs.Include(zz => zz.SALES).ToList();
            return getAllFirst_NameWithDateOFpurchase(customers);
        }
        private List<dynamic> getAllFirst_NameWithDateOFpurchase(List<CUSTOMER> forSeller)
        {
            List<dynamic> dynamicCUSTOMERs = new List<dynamic>();
            foreach (CUSTOMER customer in forSeller)
            {
                dynamic obForSeller = new ExpandoObject();
                obForSeller.Customer_ID = customer.Customer_ID;
                obForSeller.First_Name = customer.First_Name;
                obForSeller.Last_Name = customer.Last_Name;
                obForSeller.Date_Of_purchase = getDate(customer);
                dynamicCUSTOMERs.Add(obForSeller);

            }
            return dynamicCUSTOMERs;
        }
        private List<dynamic> getDate(CUSTOMER sales)
        {
            List<dynamic> dynamicDate = new List<dynamic>();
            foreach (SALE date in sales.SALES)

            {
                dynamic dynamicDa = new ExpandoObject();
                dynamicDa.Purchase_Number = date.Purchase_Number;
                dynamicDa.Date_Of_Purchase = date.Date_Of_Purchase;
                dynamicDate.Add(dynamicDa);
            }
            return dynamicDate;
        }
        /* add the list of first names*/
        [System.Web.Http.Route("api/Customer/AddAllFirst_NameWithUnitPrice")]
        [System.Web.Mvc.HttpPost]
        public List<dynamic> AddAllFirst_NameWithUnitPrice([FromBody] List<CUSTOMER> Names)
        {
            SALESEntities db = new SALESEntities();
            db.Configuration.ProxyCreationEnabled = false;
            db.CUSTOMERs.AddRange(Names);
            db.SaveChanges();
            return getAllFirst_NameWithUnitPrice();
        }

        /* Api function adds a single first name to the database*/

        [System.Web.Http.Route("api/Customer/AddAllFirst_NameWithUnitPrice")]
        [System.Web.Mvc.HttpPost]
        public List<dynamic> AddAllFirst_NameWithUnitPrice([FromBody] CUSTOMER Name)
        {
            if (Name != null)
            {
                SALESEntities db = new SALESEntities();
                db.Configuration.ProxyCreationEnabled = false;
                db.CUSTOMERs.Add(Name);
                db.SaveChanges();
                return getAllFirst_Names();
            }
            else
            {
                return null;
            }
        }
        // delete firstnames
        [System.Web.Http.Route("api/Customer/deleteFirstNames")]
        [System.Web.Mvc.HttpPost]

        public List<dynamic> deleteFirstNames([FromBody]int id)
        {
            SALESEntities db = new SALESEntities();
            db.Configuration.ProxyCreationEnabled = false;
            var data = new CUSTOMER { Customer_ID = id };
            db.CUSTOMERs.Attach(data);
            db.CUSTOMERs.Remove(data);
            db.SaveChanges();
            return getAllFirst_Names();
        }
        [System.Web.Http.Route("api/Customer/updateCustomer")]
        [System.Web.Mvc.HttpPost]

        public List<dynamic> updateCustomer([FromBody]int id,string firstname,string lastname,string numberofcomplaints)
        {
            SALESEntities db = new SALESEntities();
            db.Configuration.ProxyCreationEnabled = false;
            var update = db.CUSTOMERs.Where(s => s.Customer_ID == id).FirstOrDefault();
            update.First_Name = firstname;
            update.Last_Name = lastname;
            update.Number_Of_Complaints = numberofcomplaints;

            db.SaveChanges();

            return getAllFirst_Names();
        }
        //public ActionResult Index()11

        //ViewBag.Title = "Home Page";

        //return View();

    }
}
